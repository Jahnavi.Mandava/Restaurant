package com.example.sharp.restaurant

import android.content.Context
import android.content.res.TypedArray
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Gallery
import android.widget.ImageView

class ImageAdapter(private val mContext: Context) : BaseAdapter() {
    internal var mGalleryItemBackground: Int = 0

    private val mImageIds = arrayOf<Int>(R.drawable.anchovies, R.drawable.bacon, R.drawable.bananapepper, R.drawable.blackolives, R.drawable.chicken, R.drawable.greenpeppers, R.drawable.ham, R.drawable.jalapenopeppers, R.drawable.mozzarella, R.drawable.mushrooms, R.drawable.onion, R.drawable.pepperoni, R.drawable.pineapple, R.drawable.sausage, R.drawable.tomatoes)

    init {
        val a = mContext.obtainStyledAttributes(R.styleable.HelloGallery)
        mGalleryItemBackground = a.getResourceId(
                R.styleable.HelloGallery_android_galleryItemBackground, 0)
        a.recycle()
    }

    override fun getCount(): Int {
        return mImageIds.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val i = ImageView(mContext)

        i.setImageResource(mImageIds[position])
        i.layoutParams = Gallery.LayoutParams(150, 100)
        i.scaleType = ImageView.ScaleType.FIT_XY
        i.setBackgroundResource(mGalleryItemBackground)

        return i
    }
}
