package com.example.sharp.restaurant

import android.provider.BaseColumns

interface Constants : BaseColumns {
    companion object {
        val TABLE_NAME = "activity_main"

        // Columns in the Events database
        val SIZE = "size"
        val CRUST = "crust"
        val TOPPINGS_WHOLE = "toppingsWhole"
        val TOPPINGS_LEFT = "toppingsLeft"
        val TOPPINGS_RIGHT = "toppingsRight"
    }
}
