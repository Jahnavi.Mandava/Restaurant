package com.example.sharp.restaurant


import android.provider.BaseColumns._ID
import java.nio.file.Files.delete
//import javax.swing.UIManager.put

import android.R.menu
import android.app.AlertDialog
import android.app.ListActivity
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.SimpleCursorAdapter
import android.widget.TextView
import com.example.sharp.restaurant.Constants.Companion.CRUST
import com.example.sharp.restaurant.Constants.Companion.SIZE
import com.example.sharp.restaurant.Constants.Companion.TABLE_NAME
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_LEFT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_RIGHT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_WHOLE

class OrderPage : ListActivity(), OnClickListener {
     var data: PizzaData? = null
     var hasShown = false
     var totalText: TextView? = null
     var cursorEdit: Cursor? = null

     val events: Cursor
        get() {
            val db = data!!.getReadableDatabase()
            val cursor = db.query(TABLE_NAME, null, null, null, null, null, ORDER_BY)
            startManagingCursor(cursor)
            return cursor
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_page)
        listView.choiceMode = 1

        listView.onItemClickListener = object : OnItemClickListener {

            fun onNothingSelected(arg0: AdapterView<*>) {

            }

            override fun onItemClick(arg0: AdapterView<*>, arg1: View, arg2: Int, arg3: Long) {
                cursorEdit = listView.getItemAtPosition(arg2) as Cursor
                editPizza()
            }

        }

        data = PizzaData(this)
        try {
            val cursor = events
            showEvents(cursor)
        } finally {
            data!!.close()
        }

        // Set up click listeners for all the buttons
        val getNewPizzaButton = findViewById<View>(R.id.new_pizza_button)
        getNewPizzaButton.setOnClickListener(this)
        val getCheckoutButton = findViewById<View>(R.id.checkout_button)
        getCheckoutButton.setOnClickListener(this)
        totalText = findViewById<View>(R.id.total) as TextView
    }

    override fun onResume() {
        super.onResume()
        try {
            val cursor = events
            showEvents(cursor)
        } finally {
            data!!.close()
        }
        if (listView.count == 0)
            totalText!!.text = "Current Total: $0.00"
        else
            totalText!!.text = "Current Total: $" + listView.count * 9.99
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.new_pizza_button -> if (!hasShown) {
                openHowToDialog()
                hasShown = true
            } else {
                openSizeSelectionDialog()
            }
            R.id.checkout_button -> if (listView.count != 0)
                checkOutDialog()
            else {
                AlertDialog.Builder(this).setTitle("Info").setMessage("You must order a activity_main, before you can checkout.").setCancelable(false)
                        .setNeutralButton("OK") { dialog, which -> openSizeSelectionDialog() }.show()
            }
        }// More buttons go here (if any) ...
    }

     fun openHowToDialog() {
        AlertDialog.Builder(this).setTitle(R.string.how_to_title).setMessage(R.string.how_to_text).setCancelable(false)
                .setNeutralButton("OK") { dialog, which -> openSizeSelectionDialog() }.show()
    }

     fun openSizeSelectionDialog() {
        AlertDialog.Builder(this).setTitle(R.string.pizza_size).setItems(R.array.pizza_size) { dialog, which ->
            val size: String
            if (which == 0)
                size = "Small"
            else if (which == 1)
                size = "Medium"
            else if (which == 2)
                size = "Large"
            else
                size = "Party"
            openCrustSelectionDialog(size)
        }.show()
    }

     fun openCrustSelectionDialog(size: String) {
        AlertDialog.Builder(this).setTitle(R.string.crust_selection).setItems(R.array.pizza_crust) { dialog, which ->
            val crust: String
            if (which == 0)
                crust = "Thin"
            else if (which == 1)
                crust = "Thick"
            else if (which == 2)
                crust = "Deepdish"
            else
                crust = "Stuffed"
            addEvent(size, crust, "none", "none", "none")
            startPizzaCreation()
        }.show()
    }

     fun startPizzaCreation() {
        //		int currentPizza = cursorEdit.getInt(0);
        //		Bundle bundle = new Bundle();
        //		bundle.putInt("ID", currentPizza);

        val intent = Intent(this@OrderPage, NewPizza::class.java)
        //		intent.putExtras(bundle);
        startActivity(intent)
    }

     fun checkOutDialog() {
        AlertDialog.Builder(this).setTitle(R.string.checkout_title).setMessage(R.string.checkout_text).setCancelable(false)
                .setNeutralButton("OK") { dialog, which ->
                    data!!.getWritableDatabase().delete(TABLE_NAME, null, null)
                    finish()
                }.show()
    }

     fun addEvent(size: String, crust: String, toppingsWhole: String, toppingsLeft: String, toppingsRight: String) {
        val db = data!!.getWritableDatabase()
        val values = ContentValues()
        values.put(SIZE, size)
        values.put(CRUST, crust)
        values.put(TOPPINGS_WHOLE, toppingsWhole)
        values.put(TOPPINGS_LEFT, toppingsLeft)
        values.put(TOPPINGS_RIGHT, toppingsRight)
        db.insertOrThrow(TABLE_NAME, null, values)
    }

     fun showEvents(cursor: Cursor) {
        val adapter = SimpleCursorAdapter(this, R.layout.listview, cursor, FROM, TO)
        listAdapter = adapter
    }

     fun editPizza() {
        val currentPizza = cursorEdit!!.getInt(0)
        val bundle = Bundle()
        bundle.putInt(_ID, currentPizza)

        val intent = Intent(this@OrderPage, NewPizza::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings ->
                //			startActivity(new Intent(this, Prefs.class));
                return true
            R.id.help -> {
                AlertDialog.Builder(this).setTitle(R.string.help_title).setMessage(R.string.edit_order_help).setCancelable(false)
                        .setNeutralButton("OK") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
            R.id.exit -> {
                AlertDialog.Builder(this).setTitle(R.string.exit).setMessage("Are you sure you want to exit?").setCancelable(true)
                        .setNeutralButton("Yes") { dialog, which -> System.exit(0) }.setNegativeButton("No") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
        }
        return false
    }

    companion object {

        private val FROM = arrayOf<String>(SIZE, CRUST, TOPPINGS_WHOLE, TOPPINGS_LEFT, TOPPINGS_RIGHT)
        private val ORDER_BY = SIZE + " DESC"

        private val TO = intArrayOf(R.id.email_url, R.id.comment, R.id.wholePizzaHeader, R.id.leftHalfOnly, R.id.rightHalfOnly)
    }

}
