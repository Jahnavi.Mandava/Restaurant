package com.example.sharp.restaurant

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns._ID
//import com.example.sharp.restaurant.Constants.CRUST
import com.example.sharp.restaurant.Constants.Companion.CRUST
import com.example.sharp.restaurant.Constants.Companion.SIZE
import com.example.sharp.restaurant.Constants.Companion.TABLE_NAME
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_LEFT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_RIGHT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_WHOLE


class PizzaData(context: Context)// TODO Auto-generated constructor stub
    : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SIZE
                + " TEXT NOT NULL, " + CRUST + " TEXT NOT NULL, " + TOPPINGS_WHOLE + " TEXT, " + TOPPINGS_LEFT +
                " TEXT, " + TOPPINGS_RIGHT + " TEXT);")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    companion object {
        private val DATABASE_NAME = "activity_main.db"
        private val DATABASE_VERSION = 4
    }


}
