package com.example.sharp.restaurant

import android.provider.BaseColumns._ID

import java.nio.file.Files.delete
//import javax.UIManager.put

import java.util.ArrayList

import android.R.menu
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.Gallery
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import com.example.sharp.restaurant.Constants.Companion.SIZE
import com.example.sharp.restaurant.Constants.Companion.TABLE_NAME
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_LEFT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_RIGHT
import com.example.sharp.restaurant.Constants.Companion.TOPPINGS_WHOLE

class NewPizza : Activity(), OnClickListener {

    private var wholeRadio: RadioButton? = null
    private var leftRadio: RadioButton? = null
    private var wholeText: TextView? = null
    private var leftText: TextView? = null
    private var rightText: TextView? = null
    private val wList = ArrayList<String>()
    private val lList = ArrayList<String>()
    private val rList = ArrayList<String>()
    private val topingList = ArrayList<String>()
    private var data: PizzaData? = null
    internal var id = 999

    private val pizza: Cursor
        get() {
            val db = data!!.getReadableDatabase()
            val cursor = db.query(TABLE_NAME, null, "$_ID='$id'", null, null, null, ORDER_BY)
            startManagingCursor(cursor)
            return cursor
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_pizza)
        data = PizzaData(this)
        createToppingList()

        val bundle = this.intent.extras
        if (bundle != null && bundle.size() > 0)
            id = bundle.getInt(_ID)

        wholeRadio = findViewById<View>(R.id.whole) as RadioButton
        leftRadio = findViewById<View>(R.id.left) as RadioButton
        wholeText = findViewById<View>(R.id.whole_text) as TextView
        leftText = findViewById<View>(R.id.left_text) as TextView
        rightText = findViewById<View>(R.id.right_text) as TextView

        if (id != 999) {
            val pizzaCursor = pizza
            if (pizzaCursor.moveToFirst()) {
                wholeText!!.text = pizzaCursor.getString(pizzaCursor.getColumnIndex(TOPPINGS_WHOLE))
                leftText!!.text = pizzaCursor.getString(pizzaCursor.getColumnIndex(TOPPINGS_LEFT))
                rightText!!.text = pizzaCursor.getString(pizzaCursor.getColumnIndex(TOPPINGS_RIGHT))
                val wholeToppings = wholeText!!.text.toString().split("[,]+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                setArrayList(wList, wholeToppings)
                val leftToppings = leftText!!.text.toString().split("[,]+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                setArrayList(lList, leftToppings)
                val rightToppings = rightText!!.text.toString().split("[,]+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                setArrayList(rList, rightToppings)
            }
        }

        val gallery = findViewById<View>(R.id.gallery) as Gallery
        gallery.adapter = ImageAdapter(this)

        gallery.onItemClickListener = OnItemClickListener { parent, v, position, id ->
            if (wList.isEmpty())
                wList.clear()
            if (lList.isEmpty())
                lList.clear()
            if (rList.isEmpty())
                rList.clear()
            if (wholeRadio!!.isChecked) { // Whole Pizza
                if (wList.contains(topingList[position])) {
                    displayMessage(position, " removed")
                    wList.removeAt(wList.indexOf(topingList[position]))
                    if (wList.isEmpty())
                        wholeText!!.text = ""
                    else
                        wholeText!!.text = editString(wList)
                } else if (rList.contains(topingList[position])) {
                    rList.removeAt(rList.indexOf(topingList[position]))
                    wList.add(topingList[position])
                    rightText!!.text = editString(rList)
                    wholeText!!.text = editString(wList)
                    displayMessage(position, " added")
                } else if (lList.contains(topingList[position])) {
                    lList.removeAt(lList.indexOf(topingList[position]))
                    wList.add(topingList[position])
                    leftText!!.text = editString(lList)
                    wholeText!!.text = editString(wList)
                    displayMessage(position, " added")
                } else {
                    displayMessage(position, " added")
                    wList.add(topingList[position])
                    wholeText!!.text = editString(wList)
                }
            } else if (leftRadio!!.isChecked) { // Left side of activity_main.
                if (lList.contains(topingList[position])) {
                    displayMessage(position, " removed")
                    lList.removeAt(lList.indexOf(topingList[position]))
                    if (lList.isEmpty())
                        leftText!!.text = ""
                    else
                        leftText!!.text = editString(lList)
                } else if (rList.contains(topingList[position])) { // Checking if the same topping is on the right side of activity_main.
                    displayMessage(position, " added to whole activity_main")
                    rList.removeAt(rList.indexOf(topingList[position]))
                    rightText!!.text = editString(rList)
                    wList.add(topingList[position])
                    wholeText!!.text = editString(wList)
                } else if (wList.contains(topingList[position])) {
                    displayMessage(position, " have already been added to the whole activity_main")
                } else {
                    displayMessage(position, " added")
                    lList.add(topingList[position])
                    leftText!!.text = editString(lList)
                }
            } else { // Right side of activity_main.
                if (rList.contains(topingList[position])) {
                    displayMessage(position, " removed")
                    rList.removeAt(rList.indexOf(topingList[position]))
                    if (rList.isEmpty())
                        rightText!!.text = ""
                    else
                        rightText!!.text = editString(rList)
                } else if (lList.contains(topingList[position])) { // Checking if the same topping is on the left side of activity_main.
                    displayMessage(position, " added to whole activity_main")
                    lList.removeAt(lList.indexOf(topingList[position]))
                    leftText!!.text = editString(lList)
                    wList.add(topingList[position])
                    wholeText!!.text = editString(wList)
                } else if (wList.contains(topingList[position])) {
                    displayMessage(position, " have already been added to the whole activity_main")
                } else {
                    displayMessage(position, " added")
                    rList.add(topingList[position])
                    rightText!!.text = editString(rList)
                }
            }
        }

        val getAddToCartButton = findViewById<View>(R.id.add_to_cart_button)
        getAddToCartButton.setOnClickListener(this)
        val getCancelButton = findViewById<View>(R.id.cancel_button)
        getCancelButton.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.add_to_cart_button -> {
                addToCart()
                finish()
            }
            R.id.cancel_button -> {
                if (id == 999)
                    data!!.getWritableDatabase().delete(TABLE_NAME, TOPPINGS_WHOLE + "='none'", null)
                else
                    data!!.getWritableDatabase().delete(TABLE_NAME, "$_ID='$id'", null)
                finish()
            }
        }

    }

    private fun addToCart() {
        var toppingsWhole = ""
        var toppingsLeft = ""
        var toppingsRight = ""
        if (wList.size > 0) {
            toppingsWhole = toppingsWhole + editString(wList)
        }
        if (lList.size > 0) {
            toppingsLeft = toppingsLeft + editString(lList)
        }
        if (rList.size > 0) {
            toppingsRight = toppingsRight + editString(rList)
        }
        updatePizza(toppingsWhole, toppingsLeft, toppingsRight)
    }

    private fun createToppingList() {
        topingList.add("Anchovies")
        topingList.add("Bacon")
        topingList.add("Banana Peppers")
        topingList.add("Black Olives")
        topingList.add("Chicken")
        topingList.add("Green Peppers")
        topingList.add("Ham")
        topingList.add("Jalapeno Peppers")
        topingList.add("Extra Cheese")
        topingList.add("Mushrooms")
        topingList.add("Onion")
        topingList.add("Pepperoni")
        topingList.add("Pineapple")
        topingList.add("Sausage")
        topingList.add("Roma Tomatoes")
    }

    private fun displayMessage(position: Int, message: String) {
        Toast.makeText(this@NewPizza, topingList[position] + message, Toast.LENGTH_SHORT).show()
    }

    private fun editString(list: List<String>): String {
        var toppings = ""
        val withOutComma: String
        for (item in list) {
            toppings += "$item, "
        }
        if (toppings == "")
            withOutComma = ""
        else
            withOutComma = toppings.substring(0, toppings.length - 2)
        return withOutComma
    }

    private fun updatePizza(toppingsWhole: String, toppingsLeft: String, toppingsRight: String) {
        val db = data!!.getWritableDatabase()
        val values = ContentValues()
        values.put(TOPPINGS_WHOLE, toppingsWhole)
        values.put(TOPPINGS_LEFT, toppingsLeft)
        values.put(TOPPINGS_RIGHT, toppingsRight)
        if (id == 999)
            db.update(TABLE_NAME, values, TOPPINGS_WHOLE + "='none'", null)
        else
            db.update(TABLE_NAME, values, "$_ID='$id'", null)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                // startActivity(new Intent(this, Prefs.class));
                return true
            }
            R.id.help -> {
                AlertDialog.Builder(this).setTitle(R.string.help_title).setMessage(R.string.add_ingredients_help).setCancelable(false)
                        .setNeutralButton("OK") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
            R.id.exit -> {
                AlertDialog.Builder(this).setTitle(R.string.exit).setMessage("Are you sure you want to exit?").setCancelable(true)
                        .setNeutralButton("Yes") { dialog, which -> System.exit(0) }.setNegativeButton("No") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
        }
        return false
    }

    private fun setArrayList(list: ArrayList<String>, toppings: Array<String>) {
        for (i in toppings.indices) {
            list.add(toppings[i].trim { it <= ' ' })
        }
    }

    companion object {

        private val ORDER_BY = SIZE + " DESC"
    }

}
