package com.example.sharp.restaurant
import com.example.sharp.restaurant.R;

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity(){

     var name: EditText? = null
     var getStartedButton: Button?=null

    /** Called when the activity is first created.  */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set up click listeners for all the buttons
        getStartedButton = findViewById<Button>(R.id.get_started_button) as Button
        name = findViewById<View>(R.id.get_name) as EditText

//        val oldgrav = name!!.gravity
//        name!!.setOnKeyListener { v, keyCode, event ->
//            if (name!!.text.toString() == "")
//                name!!.gravity = oldgrav
//            else
//                name!!.gravity = Gravity.CENTER
//            false
//        }
        getStartedButton?.setOnClickListener(object :OnClickListener{
              override fun onClick(v: View) {
                        validate()
//                          if (getName() == "" || getName() == null){
//                              AlertDialog.Builder(this@MainActivity).setTitle(R.string.incomplete_title).setMessage(R.string.enter_name_text).setCancelable(false)
//                                      .setNeutralButton("OK") { dialog, which -> dialog.dismiss() }.show()}
//                          else
//                              startActivity(Intent(this@MainActivity, OrderPage::class.java))
//                          // startActivity(i)
                      }
        })


    }

      fun validate() {
         var isValidate = true

         val name = get_name.text.toString()
         if (getName() == "" || getName() == null){
                            AlertDialog.Builder(this@MainActivity).setTitle(R.string.incomplete_title).setMessage(R.string.enter_name_text).setCancelable(false)
                                      .setNeutralButton("OK") { dialog, which -> dialog.dismiss() }.show()
             isValidate=false
         }else {
             //
         }
          if(isValidate){
    startActivity(Intent(this@MainActivity,OrderPage::class.java))

}
}    fun getName(): String {
        return name!!.text.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                //			startActivity(new Intent(this, Prefs.class));
                return true
            }
            R.id.help -> {
                AlertDialog.Builder(this).setTitle(R.string.help_title).setMessage(R.string.enter_name_help).setCancelable(false)
                        .setNeutralButton("OK") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
            R.id.exit -> {
                AlertDialog.Builder(this).setTitle(R.string.exit).setMessage("Are you sure you want to exit?").setCancelable(true)
                        .setPositiveButton("Yes") { dialog, which -> System.exit(0) }.setNegativeButton("No") { dialog, which -> dialog.dismiss() }.show()
                return true
            }
        }
        return false
    }


}